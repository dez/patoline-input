# Extension of Patoline emacs input method

My extension of Patoline's emacs input method, including calligraphic,
bold, frak, mathbb fonts, plus lots of combining symbols, delimiters,
etc.

Bonus: the same as an ibus table, for system-wide macros!

There are little differences with the emacs method, because it seems
to be hard wired in ibus that "bypass ibus" is "return".  So instead
of "->" for →, you'll need to type "\->", and similarly for "-->",
"=>", "==>", "~>" (you need to type "\=>", and so on).