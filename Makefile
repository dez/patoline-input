all:
	ibus-table-createdb -d -s ibus-patoline-table.txt

install:
	cp patoline-table.svg /usr/share/ibus-table/icons/
	cp ibus-patoline-table.db /usr/share/ibus-table/tables/

restart:
	ibus-daemon -d 
